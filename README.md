# rokas-bot

Lunch?

### Deploy

1. Rename `.env.sample` to `.env` and fill the Mattermost hook URL.
2. Build the image and push it to the registry

```console
$ docker build -t gitlab-registry.cern.ch/mvidalga/rokas-bot .
$ docker push gitlab-registry.cern.ch/mvidalga/rokas-bot
```

3. Create an OpenShift project
4. Deploy on OpenShift
