FROM python:3-alpine
ENV GOCROND_VERSION 0.6.1
ENV TZ Europe/Zurich
RUN set -ex && \
	apk add --update curl tzdata && \
	curl -fsSL -o /usr/local/bin/go-crond https://github.com/webdevops/go-crond/releases/download/$GOCROND_VERSION/go-crond-64-linux && \
	chmod +x /usr/local/bin/go-crond && \
	apk del curl && \
	rm -rf /var/cache/apk/*
COPY .env rokas.py crontab requirements.txt /srv/rokas-bot/
WORKDIR /srv/rokas-bot
RUN pip install -r requirements.txt
# gocrond doesn't switch users in unpriv'd mode, but we still need to
# specify one so it doesn't expect one inside the crontab itself
CMD ["/usr/local/bin/go-crond", "-v", "--allow-unprivileged", "--no-auto", "johndoe:/srv/rokas-bot/crontab"]
