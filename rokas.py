import os
import sys

import requests
from dotenv import load_dotenv
load_dotenv()

try:
    # mattermost webhook url
    MMHOOK = os.environ['MMHOOK']

except KeyError as exc:
    print(f'Required environment variable not set: {exc}')
    sys.exit(2)


def main():
    discord_channel = \
        'https://discordapp.com/channels/688128312152948763/688128312782225478'
    requests.post(MMHOOK, json={
        'text': f'[Lunch?]({discord_channel})',
        'icon_url': 'https://mattermost.web.cern.ch/api/v4/users/caaam9mrobdg7ycsriskzaufoa/image?_=0',
        'username': 'Rokas Maciulaitis'})


if __name__ == '__main__':
    print('Sending message...')
    main()
